package ee.bcs.valiit.Yl6.Athlete;

public class Runner extends Athlete { // laiendab Yl6
    public Runner() {
        super();
    }

    public Runner (String name, String surname, int age){
        super(name, surname, age); // super on see, mida ekstendib
    }

    @Override
    public void perform() { // overriden seda
        System.out.println("Running ...");
    }
}
package ee.bcs.valiit.Yl6.Athlete;

public class SkyDiver extends Athlete {
    public SkyDiver() {
        super();
    }

    public SkyDiver (String name, String surname, int age){
        super(name, surname, age);
    }
    @Override
    public void perform() {
        System.out.println("Jumping out of the plane ...");
    }
}
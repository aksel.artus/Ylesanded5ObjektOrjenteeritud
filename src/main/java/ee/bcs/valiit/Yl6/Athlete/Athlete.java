package ee.bcs.valiit.Yl6.Athlete;

import lombok.Data;

@Data // ei pea getterit ega setterit kirjutama enam

public class Athlete {

    protected String name;
    protected String surname;
    protected int age;
    protected Gender gender;
    protected double height;
    protected double weight;

    public Athlete() {
        super();
    }// et säiliks, on vaja ühte tühja atribuuti

    public Athlete (String name, String surname, int age) {  // teeme konstruktori - võtab 3 argumenti sisse ja omistab objektile
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void perform() {} // overrideme toString meetodi. Igal klassil object toString ja seda saab overideda. Kuna me peame kõik overrideme, siis mõistlik teha seda baasklassis

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder(name)//tagastan stringi kus kõik atribuudid välja toodud
                .append(", ")
                .append(surname)
                .append(", ")
                .append(age)
                .append(", ")
                .append(gender)
                .append(", ")
                .append("height")
                .append(", ")
                .append("weight");
        return builder.toString();

    }
}
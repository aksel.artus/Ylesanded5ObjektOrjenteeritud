package ee.bcs.valiit.Yl6.Athlete;

public class AthleteApp {
    public static void main(String[] args) {
        Runner runner1 = new Runner("Mati", "Jooksja", 31);

        //runner1.setName("Mati");
        //runner1.setSurname("Jooksja");
        //runner1.setAge(31);
        runner1.setGender(Gender.MALE);
        runner1.setHeight(180.0);
        runner1.setWeight(90.5);

        Runner runner2 = new Runner();
        runner2.setName("Turbo");
        runner2.setSurname("Jalg");
        runner2.setAge(47);
        runner2.setGender(Gender.MALE);
        runner2.setHeight(190.0);
        runner2.setWeight(86.5);
        Runner runner3 = new Runner();
        runner3.setName("Tigu");
        runner3.setSurname("Mudas");
        runner3.setAge(99);
        runner3.setGender(Gender.FEMALE);
        runner3.setHeight(10.0);
        runner3.setWeight(1.5);

        SkyDiver skydiver1 = new SkyDiver();
        skydiver1.setName("Lange");
        skydiver1.setSurname("Vari");
        skydiver1.setAge(25);
        skydiver1.setGender(Gender.FEMALE);
        skydiver1.setHeight(200.0);
        skydiver1.setWeight(69.5);

        SkyDiver skydiver2 = new SkyDiver();
        skydiver2.setName("Hele");
        skydiver2.setSurname("Laks");
        skydiver2.setAge(3);
        skydiver2.setGender(Gender.FEMALE);
        skydiver2.setHeight(1000.0);
        skydiver2.setWeight(99.9);

        SkyDiver skydiver3 = new SkyDiver();
        skydiver3.setName("Prillid");
        skydiver3.setSurname("Peas");
        skydiver3.setAge(57);
        skydiver3.setGender(Gender.MALE);
        skydiver3.setHeight(50.0);
        skydiver3.setWeight(89.5);

        System.out.println(runner1);
        runner1.perform();
        System.out.println(runner2);
        runner2.perform();
        System.out.println(runner3);
        runner3.perform();
        System.out.println(skydiver1);
        skydiver1.perform();
        System.out.println(skydiver2);
        skydiver2.perform();
        System.out.println(skydiver3);
        skydiver3.perform();
    }
}
package ee.bcs.valiit.Yl6.Ex2_Ex3;

public class Latvia extends CountryInfo {
    public Latvia() {
        super();
    }

    public Latvia(String country, String primeMinister, String languages) {
        super(country, primeMinister, languages);

    }

    @Override
    public void call() { // overriden seda
        System.out.println("Lätlane ütleb: Saldejumps");
    }
}



package ee.bcs.valiit.Yl6.Ex2_Ex3;

public class Estonia extends CountryInfo {
    public Estonia() {
        super();
    }

    public Estonia(String country, String primeMinister, String languages) {
        super(country, primeMinister, languages);

    }

    @Override
    public void call() { // overriden seda
        System.out.println("Eestlane ütleb: Tere!");
    }
}



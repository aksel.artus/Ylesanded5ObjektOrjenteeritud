package ee.bcs.valiit.Yl6.Ex2_Ex3;


import lombok.Data;

@Data
public class CountryInfo {

    protected String country;
    protected String primeMinister;
    protected String languages;

    public CountryInfo() {
        super();
    }

    public CountryInfo(String country, String primeMinister, String languages) {
        this.country = country;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    public void call() {

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(country)
                .append(": ")
                .append(languages);
        return builder.toString();


    }
}

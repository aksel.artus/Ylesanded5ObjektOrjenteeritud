package ee.bcs.valiit.Yl6.Ex2_Ex3;

public class CountryApp {
    public static void main(String[] args) {
        Estonia est1 = new Estonia("Estonia", "Jüri Ratas", " \n   Estonian\n   Russian\n   Ukrainian\n   Belarusian\n   Finnish");
        System.out.println(est1);
        est1.call();
        Latvia lat1 = new Latvia("Latvia", "Māris Kučinskis", "\n   Latvian\n   Russian\n   Ukrainian\n   Belarusian\n   Polish");
        System.out.println(lat1);
        lat1.call();
    }
}
